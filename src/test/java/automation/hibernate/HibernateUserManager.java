package automation.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUserManager {
    private SessionFactory sessionFactory; //manages sessions, creates instances, instances manage crud
    public HibernateUserManager() {
    }

    public void connectToDB() {
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder() //holds configuration settings and services required by Hibernate to interact with the database
                .configure()
                .build();
        try {
            sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

    public void disconnectFromDB() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }

    public void deleteUserFromDB(String usernameToDelete) {

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();

            User userToDelete = session.get(User.class, usernameToDelete);

            if (userToDelete != null) {
                session.delete(userToDelete);
                session.getTransaction().commit();
                System.out.println(usernameToDelete + " user deleted successfully.");
            } else {
                System.out.println(usernameToDelete + " user not found in the database.");
            }
        }
    }

    public void addUserToDB(String userAdd, String password) {

        Session session = sessionFactory.openSession();
        session.beginTransaction();

        User userToAdd = session.get(User.class, userAdd);

        if (userToAdd == null) {
            User user = new User(userAdd, password);
            session.persist(user);
            session.getTransaction().commit();
        } else {
            System.out.println(userAdd + " user already exists!");
        }
    }
}
