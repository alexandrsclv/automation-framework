package automation.glue;

import automation.config.AutomationFrameworkConfiguration;
import automation.drivers.DriverSingleton;
import automation.hibernate.HibernateUserManager;
import automation.utils.ConfigurationProperties;
import io.cucumber.java.After;
import io.cucumber.java.AfterAll;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = AutomationFrameworkConfiguration.class)
public class Hooks {

    @Autowired
    ConfigurationProperties configurationProperties;
    @BeforeAll
    public static void setUpUsers() throws Exception {
        HibernateUserManager hibernateUserManager = new HibernateUserManager();

        hibernateUserManager.connectToDB();
        hibernateUserManager.deleteUserFromDB("testing_signUp");
        hibernateUserManager.addUserToDB("testing_signIn", "test");
        hibernateUserManager.disconnectFromDB();
    }
    @Before(value = "@ui")
    public void initializeObjects() {
        DriverSingleton.getInstance(configurationProperties.getBrowser());
    }

    @After(value = "@ui")
    public void closeObjects(){
        DriverSingleton.closeObjectInstance();
    }

    @AfterAll
    public static void cleanUpUsers() throws Exception{
        HibernateUserManager hibernateUserManager = new HibernateUserManager();
        hibernateUserManager.connectToDB();
        hibernateUserManager.deleteUserFromDB("testing_signUp");
        hibernateUserManager.deleteUserFromDB("testing_signIn");
        hibernateUserManager.disconnectFromDB();
    }
}