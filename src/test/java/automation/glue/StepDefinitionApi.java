package automation.glue;

import automation.api.Context;
import automation.api.ApiMethods;
import automation.api.ScenarioContext;
import automation.config.AutomationFrameworkConfiguration;
import automation.utils.Log;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import java.util.List;

@ContextConfiguration (classes = AutomationFrameworkConfiguration.class)
public class StepDefinitionApi {

    @Autowired
    ScenarioContext scenarioContext;
    @Autowired
    ApiMethods apiMethods;
    private List<String> cookies;

    @Given("^I make a GET request to access Websites main page")
    public void i_make_a_get_request_to_access_websites_main_page() {
        cookies = apiMethods.setCookie();
        if (scenarioContext != null) {
            scenarioContext.setCookies(cookies);
        } else {
            throw new IllegalStateException("ScenarioContext is not initialized");
        }
    }

    @When("^I insert my credentials and make a POST request")
    public void i_insert_my_credentials_and_make_a_post_request() {
        List<String> cookies = (List<String>) scenarioContext.getContext(Context.COOKIES);
        apiMethods.sendCredentials(cookies);
    }

    @Then("^I can GET the authenticated user page")
    public void i_can_get_the_authenticated_user_page() {
        List<String> cookies = (List<String>) scenarioContext.getContext(Context.COOKIES);
        apiMethods.getHomePage(cookies);
        Log.info("Log In thru API passed.");
    }
}
