package automation.glue;

import automation.config.AutomationFrameworkConfiguration;
import automation.drivers.DriverSingleton;
import automation.pages.*;
import automation.utils.*;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.CucumberContextConfiguration;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

@CucumberContextConfiguration
@ContextConfiguration (classes = AutomationFrameworkConfiguration.class)
public class StepDefinition {

    @Autowired
    ConfigurationProperties configurationProperties;
    private WebDriver driver;
    private GuestHomePage guestHomePage;
    private HomePage homePage;
    private LogInPage logInPage;
    private SignUpPage signUpPage;

    @Given("^I go to the Website")
    public void i_go_to_the_website(){
        driver = DriverSingleton.getDriver();
        driver.get(Constants.URL);
        Log.info("Navigating to " + Constants.URL);
    }

    @And("^I click on sign in button")
    public void i_click_on_sign_in_button(){
        Log.startTest(TestCases.T2.getTestName());
        guestHomePage = new GuestHomePage();
        guestHomePage.clickLogIn();
    }

    @And("^I click on sign up button")
    public void i_click_on_sign_up_button(){
        Log.startTest(TestCases.T1.getTestName());
        guestHomePage = new GuestHomePage();
        guestHomePage.clickSignUp();
    }

    @When("^I specify my credentials and click Login")
    public void i_specify_my_credentials_and_click_login(){
        logInPage = new LogInPage();
        WaitUtility.waitForElementToBeClickable(driver, logInPage.getLogInUsername());
        logInPage.logIn(configurationProperties.getUsernameLogin(), configurationProperties.getPassword());
        logInPage.refreshLoginPage();
        Log.info("Credentials are introduced and Login button clicked");
    }

    @When("^I specify my credentials and click Sign Up")
    public void i_specify_my_credentials_and_click_sign_up(){
        signUpPage = new SignUpPage();
        WaitUtility.waitForElementToBeClickable(driver, signUpPage.getSignUpUsername());
        signUpPage.signUp(configurationProperties.getUsername(), configurationProperties.getPassword());
        Log.info("Credentials are introduced and Sign Up button is clicked");
    }

    @When("^I specify invalid credentials and click Login")
    public void i_specify_invalid_credentials_and_click_login(){
        logInPage = new LogInPage();
        WaitUtility.waitForElementToBeClickable(driver, logInPage.getLogInUsername());
        logInPage.logIn("InvalidUser", "test");
    }

    @Then("^I can log into the website")
    public void i_can_log_into_the_website(){
        guestHomePage = new GuestHomePage();
        homePage = new HomePage();
        WaitUtility.waitForTextToBePresent(driver, homePage.getUsernameElement(), "testing_signIn");
        Utils.takeScreenshot("Log_in");
        assertEquals("Logged in as: " + configurationProperties.getUsernameLogin(), guestHomePage.getUsername());
        Log.info("User is Logged In");
        Log.endTest(TestCases.T2.getTestName());
    }

    @Then("^an account is created")
    public void an_account_is_created(){
        signUpPage = new SignUpPage();
        WaitUtility.waitForTextToBePresent(driver, signUpPage.getAccountCreated(), "Account created");
        Utils.takeScreenshot("Account_created");
        assertEquals("Account created", signUpPage.getAccount());
        Log.endTest(TestCases.T1.getTestName());
    }

    @Then("^I get an error that my user already exists")
    public void i_get_an_error_that_my_user_already_exists(){
        signUpPage = new SignUpPage();
        assertEquals("Please enter your details to sign up", signUpPage.getUsernameError());
        Utils.takeScreenshot("User_exists");
        Log.info("Sign Up failed.");
        Log.endTest(TestCases.T2.getTestName());
    }

    @Then("^login fails and an error appears")
    public void login_fails_and_an_error_appears(){
        guestHomePage = new GuestHomePage();
        WaitUtility.waitForTextToBePresent(driver, guestHomePage.getLoginErrorElement(), "Invalid login attempt");

        Utils.takeScreenshot("Login_fail");

        assertTrue(guestHomePage.getLoginError().contains("Invalid login attempt"));
    }
}
