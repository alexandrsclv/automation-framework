package automation.utils;

import automation.drivers.DriverSingleton;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

public class Utils {

    public static String decode64(String encodedStr) {
        Base64.Decoder decoder = Base64.getDecoder();
        return new String(decoder.decode(encodedStr.getBytes()));
    }

    private static String runFolder;

    public static boolean takeScreenshot(String stepName) {
        try {
            if (runFolder == null) {
                String runTimestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                runFolder = Constants.SCREENSHOTS_FOLDER + "screenshots_" + runTimestamp + File.separator;
                new File(runFolder).mkdirs();
            }

            File file = ((TakesScreenshot) DriverSingleton.getDriver()).getScreenshotAs(OutputType.FILE);
            String fileName = stepName + Constants.SCREENSHOTS_EXTENSION;
            FileCopyUtils.copy(file, new File(runFolder + fileName));

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
