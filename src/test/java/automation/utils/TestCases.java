package automation.utils;

public enum TestCases {
    T1("Testing the registration"),
    T2("Testing the authentication");

    private final String testName;
    TestCases(String value){
        this.testName = value;
    }

    public String getTestName() {
        return testName;
    }
}
