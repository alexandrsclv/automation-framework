package automation.utils;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Log {
    private static final Logger Logger = LogManager.getLogger(Log.class.getName());

    public static void startTest(String testName){
        Logger.info("Test called: " + testName + " has started");
    }

    public static void endTest(String testName){
        Logger.info("Test called: " + testName + " has ended");
    }

    public static void info(String message){
        Logger.info(message);
    }

    public static void warn(String message){
        Logger.warn(message);
    }

    public static void error(String message){
        Logger.error(message);
    }

    public static void fatal(String message){
        Logger.fatal(message);
    }

    public static void debug(String message){
        Logger.debug(message);
    }
}