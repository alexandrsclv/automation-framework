package automation.utils;
//mv to .properties
public class Constants {

    public static final String URL = "http://localhost:8080/socialize/";
    public static final int TIMEOUT = 60;
    public static final String SCREENSHOTS_FOLDER = "target\\";
    public static final String SCREENSHOTS_EXTENSION = ".png";
}
