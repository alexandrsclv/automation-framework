package automation.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("framework.properties")
public class ConfigurationProperties {

    @Value("${browser}")
    private String browser; // injecting values into fields

    @Value("${user}")
    private String user;

    @Value("${user_login}")
    private String userLogin;

    @Value("${password}")
    private String password;

    public String getBrowser() {
        return browser;
    }

    public String getUsername() {
        return user;
    }

    public String getUsernameLogin() {
        return userLogin;
    }

    public String getPassword() {
        return password;
    }
}
