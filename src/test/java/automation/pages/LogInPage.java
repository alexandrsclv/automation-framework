package automation.pages;

import automation.drivers.DriverSingleton;
import automation.utils.Utils;
import automation.utils.WaitUtility;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogInPage {
    private WebDriver driver;

    public LogInPage(){
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }
    @FindBy(css = "div.ui-content form input[type=text]")
    private WebElement logInUsername;
    @FindBy(css = "div.ui-content form input[type=password]")
    private WebElement logInPassword;
    @FindBy(css = "div.ui-content form input[type=submit]")
    private WebElement loginSubmit;
    @FindBy(css = "div.ui-content > div:nth-child(3) > a")
    private WebElement pageRefresh;

    public void logIn(String username, String password){
        logInUsername.sendKeys(username);
        logInPassword.sendKeys(Utils.decode64(password));
        loginSubmit.click();
    }

    public void refreshLoginPage(){
        WaitUtility.waitForElementToBeClickable(driver, pageRefresh);
        pageRefresh.click();
    }

    public WebElement getLogInUsername() {
        return logInUsername;
    }
}