package automation.pages;

import automation.drivers.DriverSingleton;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class NavigationManager {
    private WebDriver driver;

    public NavigationManager() {
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy (css = "body > div.ui-page.ui-page-theme-a.ui-page-active > div.ui-content > div > a.ui-link.ui-btn.ui-icon-home.ui-btn-icon-left.ui-btn-inline.ui-shadow.ui-corner-all")
    private WebElement home;

    @FindBy (css = "body > div.ui-page.ui-page-theme-a.ui-page-active > div.ui-content > div > a.ui-link.ui-btn.ui-icon-user.ui-btn-icon-left.ui-btn-inline.ui-shadow.ui-corner-all")
    private WebElement members;

    @FindBy (css = "body > div.ui-page.ui-page-theme-a.ui-page-active > div.ui-content > div > a.ui-link.ui-btn.ui-icon-heart.ui-btn-icon-left.ui-btn-inline.ui-shadow.ui-corner-all")
    private WebElement friends;

    @FindBy (css = "body > div.ui-page.ui-page-theme-a.ui-page-active > div.ui-content > div > a.ui-link.ui-btn.ui-icon-mail.ui-btn-icon-left.ui-btn-inline.ui-shadow.ui-corner-all")
    private WebElement messages;

    @FindBy (css = "body > div.ui-page.ui-page-theme-a.ui-page-active > div.ui-content > div > a.ui-link.ui-btn.ui-icon-edit.ui-btn-icon-left.ui-btn-inline.ui-shadow.ui-corner-all")
    private WebElement editProfile;

    @FindBy (css = "body > div.ui-page.ui-page-theme-a.ui-page-active > div.ui-content > div > a.ui-link.ui-btn.ui-icon-action.ui-btn-icon-left.ui-btn-inline.ui-shadow.ui-corner-all")
    private WebElement logOut;

    public void home(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        wait.until(ExpectedConditions.elementToBeClickable(home));
        home.click();
    }

    public void members(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        wait.until(ExpectedConditions.elementToBeClickable(members));
        members.click();
    }

    public void friends(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        wait.until(ExpectedConditions.elementToBeClickable(friends));
        friends.click();
    }

    public void messages(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        wait.until(ExpectedConditions.elementToBeClickable(messages));
        messages.click();
    }

    public void editProfile(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        wait.until(ExpectedConditions.elementToBeClickable(editProfile));
        editProfile.click();
    }

    public void logOut(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
        wait.until(ExpectedConditions.elementToBeClickable(logOut));
        logOut.click();
    }
}