package automation.pages;

import automation.drivers.DriverSingleton;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;

public class Members {
    private WebDriver driver;

    public Members() {
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }
    @FindBy (xpath = "/html/body/div[3]/div[2]/li[2]/a[1]")
    private WebElement member;

    @FindBy (xpath = "/html/body/div[3]/div[2]/li[2]/a[2]")
    private WebElement follow;

    public void viewMember(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.elementToBeClickable(member));
        member.click();
    }
    public void followMember(){
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(ExpectedConditions.elementToBeClickable(follow));
        follow.click();
    }
}