package automation.pages;

import automation.drivers.DriverSingleton;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    private WebDriver driver;

    public HomePage(){
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "body > div.ui-page.ui-page-theme-a.ui-page-active > div.ui-header.ui-bar-inherit > div.username")
    private WebElement username;

    @FindBy(css = "body > div.ui-page.ui-page-theme-a.ui-page-active > div.ui-content > a")
    private WebElement viewMessages;

    public String getUsername(){
        return username.getText();
    }

    public WebElement getUsernameElement(){
        return username;
    }
}
