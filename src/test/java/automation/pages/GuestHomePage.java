package automation.pages;

import automation.drivers.DriverSingleton;
import automation.utils.WaitUtility;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GuestHomePage {
    private WebDriver driver;

    public GuestHomePage(){
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "a.ui-link.ui-btn.ui-icon-check.ui-btn-icon-left.ui-btn-inline.ui-shadow.ui-corner-all")
    private WebElement logInButton;

    @FindBy(css = "a.ui-link.ui-btn.ui-icon-plus.ui-btn-icon-left.ui-btn-inline.ui-shadow.ui-corner-all")
    private WebElement signUpButton;

    @FindBy(css = "body > div.ui-page.ui-page-theme-a.ui-page-active > div.ui-header.ui-bar-inherit > div.username")
    private WebElement username;

    @FindBy(css = "div.ui-page.ui-page-theme-a.ui-page-active  form > div:nth-child(1) > span")
    private WebElement loginError;

    public void clickLogIn(){
        WaitUtility.waitForElementToBeClickable(driver, logInButton);
        logInButton.click();
    }

    public void clickSignUp(){
        WaitUtility.waitForElementToBeClickable(driver, signUpButton);
        signUpButton.click();
    }

    public String getUsername(){
        return username.getText();
}

    public WebElement getSignUpButton() {
        return signUpButton;
    }

    public String getLoginError() {
        return loginError.getText();
    }

    public WebElement getLoginErrorElement() {
        return loginError;
    }
}