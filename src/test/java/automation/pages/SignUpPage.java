package automation.pages;

import automation.drivers.DriverSingleton;
import automation.utils.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignUpPage {
    private WebDriver driver;

    public SignUpPage(){
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "div.ui-content form div:nth-child(2) input[type=text]")
    private WebElement signUpUsername;

    @FindBy(css = "div.ui-content form div:nth-child(3) input[type=text]")
    private WebElement signUpPassword;

    @FindBy(css = "div.ui-content form div:nth-child(4) input[type=submit]")
    private WebElement signUpSubmit;

    @FindBy(css = "div.ui-content h4")
    private WebElement accountCreated;

    @FindBy(xpath = "/html/body/div[3]/div[2]/form/div[1]")
    private WebElement usernameExists;

    public void signUp(String username, String password){
        signUpUsername.sendKeys(username);
        this.signUpPassword.sendKeys(Utils.decode64(password));
        signUpSubmit.click();
    }

    public String getUsernameError(){
        return usernameExists.getText();
    }

    public WebElement getAccountCreated(){
        return accountCreated;
    }

    public WebElement getSignUpUsername() {
        return signUpUsername;
    }

    public String getAccount(){
        return accountCreated.getText();
    }
}
