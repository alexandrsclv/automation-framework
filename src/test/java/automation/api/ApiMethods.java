package automation.api;

import automation.utils.Constants;
import org.junit.jupiter.api.Assertions;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.List;
import static org.junit.Assert.assertEquals;

@Component
public class ApiMethods {
    RestTemplate restTemplate = new RestTemplate();
    public List<String> setCookie(){
        String apiUrl = Constants.URL;
        ResponseEntity<String> response = restTemplate.getForEntity(apiUrl, String.class);
        HttpHeaders responseHeaders = response.getHeaders();
        List<String> cookies = responseHeaders.get(HttpHeaders.SET_COOKIE);
        return cookies;
    }

    public void sendCredentials(List<String> cookies){

        HttpHeaders requestHeaders = new HttpHeaders();

        requestHeaders.addAll(HttpHeaders.COOKIE, cookies);

        String url = Constants.URL + "login.php";

        HttpHeaders headers = new HttpHeaders();
        headers.addAll(HttpHeaders.COOKIE, cookies);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED); //this media type is commonly used when submitting HTML form data to a server via HTTP request

        HttpEntity<String> requestEntity = new HttpEntity<>("user=isamohvalovaTEST&pass=test", headers); //construct the request entity that is sent to the server

        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    public void getHomePage(List<String> cookies){

        String url = Constants.URL + "members.php";

        HttpHeaders headers = new HttpHeaders();
        headers.addAll(HttpHeaders.COOKIE, cookies);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<String> requestEntity = new HttpEntity<>("view=isamohvalovaTEST", headers);

        ResponseEntity<String> responseEntity = restTemplate
                .exchange(url, HttpMethod.GET, requestEntity, String.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertTrue(responseEntity.getBody()
                        .contains("<div class='username'>Logged in as: isamohvalovaTEST</div>"),
                "Response body contains 'Logged in as: isamohvalovaTEST'");
    }
}

