package automation.api;

import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ScenarioContext {

    private static Map<String, Object> scenarioContext;

    public ScenarioContext(){
        scenarioContext = new HashMap<String, Object>();
    }

    public void setContext(Context key, Object value) {
        scenarioContext.put(key.toString(), value);
    }

    public static Object getContext(Context key){
        return scenarioContext.get(key.toString());
    }

    public static void setCookies(List<String> cookies) {
        scenarioContext.put("COOKIES", cookies);
    }
}