@ui @login
Feature: Log In

  Scenario: Testing the authentication
    Given I go to the Website
    And I click on sign in button
    When I specify my credentials and click Login
    Then I can log into the website

  @negative
  Scenario: Testing the authentication for an absent user
    Given I go to the Website
    And I click on sign in button
    When I specify invalid credentials and click Login
    Then login fails and an error appears
