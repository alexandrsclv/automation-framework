@ui @signup
Feature: Sign Up

  Scenario: Testing the new user registration
    Given I go to the Website
    And I click on sign up button
    When I specify my credentials and click Sign Up
    Then an account is created

  @negative
  Scenario: Testing the existing user registration fail
    Given I go to the Website
    And I click on sign up button
    When I specify my credentials and click Sign Up
    Then I get an error that my user already exists