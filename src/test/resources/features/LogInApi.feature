Feature: Log In Api

  @api
  @login
  Scenario: Testing the authentication with API
    Given I make a GET request to access Websites main page
    When I insert my credentials and make a POST request
    Then I can GET the authenticated user page